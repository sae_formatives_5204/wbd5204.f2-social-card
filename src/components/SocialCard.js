import Button from "./Button";
import Avatar from "./Avatar";

export default ({title, tag = [], children}) => {


    return (
        <div className="card">

            {/* CARD HEADER */}
            <div className="cardHeader">
                <div>
                    <Avatar src="https://i.pravatar.cc/64" />

                    <h1>{title}</h1>
                </div>

                {/* CARD TAG */}
                <div className="cardTag">
                    {tag.map((tag, index) => (
                        <span key={index}>{tag}</span>
                    ))}
                </div>
            </div>

            {/* CARD CONTENT */}
            <div className="cardContent">
                {children}
            </div>

            {/* CARD FOOTER */}
            <div className="cardFooter">
                <Button onClick={() => window.open("www.google.ch", "_blank")} title="Voir plus" />
            </div>

        </div>
    )
}
