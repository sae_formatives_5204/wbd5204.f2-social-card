import "./assets/css/custom.scss";
import SocialCard from "./components/SocialCard";

export default () => (
    <SocialCard title="Jhon Doe" tag={["WEB","DESIGN","FRONT-END","BACK-END"]}>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus varius luctus elementum. Suspendisse congue non.</p>
    </SocialCard>
);
